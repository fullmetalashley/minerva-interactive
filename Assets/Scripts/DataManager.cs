﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

/*This script keeps track of information that needs to move between the scenes.  
 * Information in this script is accessed by multiple elements, including TimeCheck,
 * the SaveSystem, and other elements.  
 * Some of these elements are also pushed into the saved data for the game itself.
 * Please note that this script only contains data, and does not manipulate data from other elements.
 * This helps keep the script accessible from other elements regardless of what scene is currently active.*/
public class DataManager : MonoBehaviour
{
    //This tracks the current instance of the DataManager and replaces it as necessary.
    public static DataManager instance;

    //Whether or not the story map has been accessed, in order to remove the tool tip.
    public bool mapAccessed;

    //Whether or not the dressing rooms have been accessed, in order to turn off the fade.
    public bool dressingAccessed;

    //A running list of what the dolls are currently wearing so they persist during a session.
    public Dictionary<string, Sprite> elizabethAssets;
    public Dictionary<string, Sprite> janeAssets;

    public Dictionary<string, Sprite> elizabethJewelry;
    public Dictionary<string, Sprite> janeJewelry;

    //Tracks if the dolls have been accessed.
    public bool dollsSaved;
    public bool jewelrySaved;

    //Tracks whether or not a tableau has been saved and which tableau was last selected.
    public bool tableauSaved;
    public int tableauIndex;

    //Tracks which doll was dressed last to load them first.
    public string lastAccessedDoll;

    //Tracks the player's name.  Unnecessary, purely for testing.
    public string playerName;

    //Tracks the date the player last played.
    public int[] inGameSplit;
    //Tracks the date the player last played.
    public int[] realTimeSplit;

    //These two items track the previous dates from the last time the player quit the game, separate from this instance.
    public DateTime lastPlayedInGame;   //This is stored when the player saves the game.
    public DateTime lastPlayedRealTime;   //The real time last played.

    //These items track the current dates for this play session, which will be stored when the player quits.
    public DateTime currentPlayedRealTime;   //The current system date.
    public DateTime currentPlayedInGame;   //The current game date.

    //This checks to see if we have loaded player data or not.  Called from the Story Map upon start.
    //I DO NOT THINK WE NEED THIS ANYMORE.  We should be using Data Exists.
    public bool loaded;

    //This checks to see if there is any content loaded, i.e. the player has played the game before.
    public bool dataExists;

    //The directory where the tableaus are saved.  Defaults to the project folder.
    public string tableauDirectory;

    //A list of all sprites for the tableaus that have been created.
    public List<Sprite> savedTableaus;

    //The indexes for the PnP reading.
    public int pnpChapter;
    public int pnpVolume;
    public int pnpPage;

    //Indicates that the book has been read, so these values should be read.
    public bool bookRead;

    //Indicates the last scene the player was in to help utilize the back button in the jewelry room.
    public string lastScene;

    public List<bool> inactiveLettersRead = new List<bool>();
    public List<string> inactiveLetterDates = new List<string>();

    public int unreadLettersProcessed;

    public bool debugDatesActive;

    public bool signingPostcards;

    public List<DateTime> tableauDates;

    public string[] tableauDatesString;

    public bool navGlowEncountered;

    public bool mailAccessed;

    //This does a check to avoid duplicating objects across scene loads.
    private void Awake()
    {
        if (instance != null)
        {
            Destroy(this.gameObject);
        }
        else
        {
            instance = this;
            DontDestroyOnLoad(this);
        }
    }

    //Creates a new DateTime using YYYY/MM/DD format from the int list.
    public DateTime SetDate(int[] theDate)
    {
        return new DateTime(theDate[2], theDate[0], theDate[1]);
    }

    //Accesses the save system to save the current data.
    public void SaveGame()
    {
        //Set the saved inGameDate to the values of the current in-game date.
        inGameSplit = new int[3];
        inGameSplit[0] = currentPlayedInGame.Month;
        inGameSplit[1] = currentPlayedInGame.Day;
        inGameSplit[2] = currentPlayedInGame.Year;

        //Get the system date and save that.
        realTimeSplit = new int[3];
        realTimeSplit[0] = currentPlayedRealTime.Month;
        realTimeSplit[1] = currentPlayedRealTime.Day;
        realTimeSplit[2] = currentPlayedRealTime.Year;

        //We also need to save the tableau dates.
        //We have a list of the dates, so we will create an array of that length and put the dates in there.
        if (tableauDates != null)
        {
            tableauDatesString = new string[tableauDates.Count];
            for (int i = 0; i < tableauDates.Count; i++)
            {
                tableauDatesString[i] = tableauDates[i].Month + "/" + tableauDates[i].Day + "/" + tableauDates[i].Year;
            }
        }

        //Save the game.
        if (FindObjectOfType<DataManager>() != null)
        {
            FindObjectOfType<DataManager>().GetComponent<SaveSystem>().SaveData();
        }
    }

    //If the player real time values need to be reset, like when the game resets at the beginning, this method is called.
    //Currently called by the Clock Tracker.
    public void ResetRealTime()
    {
        currentPlayedRealTime = new DateTime(System.DateTime.Now.Year, System.DateTime.Now.Month, System.DateTime.Now.Day, System.DateTime.Now.Hour, System.DateTime.Now.Minute, System.DateTime.Now.Second);
        lastPlayedRealTime = new DateTime(System.DateTime.Now.Year, System.DateTime.Now.Month, System.DateTime.Now.Day, System.DateTime.Now.Hour, System.DateTime.Now.Minute, System.DateTime.Now.Second);
    }

    //This is called to set up basic player data in case they have never played the game.
    //Will only be used the first time the player opens the game, if they reset, or for debugging purposes.
    public void DefaultLoad()
    {
        //Establish that there is no existing data right now.
        //Q TO ASH: Not sure if we need this still, as of 1/22/2020.
        dataExists = false;

        //Initialize the tableau dates.
        tableauDates = new List<DateTime>();

        //Set the dates for the game.
        //Currently set to 10/05/1811 for debugging.
        //Should be 09/29/1811 as the standard.
        currentPlayedInGame = new DateTime(1811, 09, 29);
        lastPlayedInGame = new DateTime(1811, 09, 29);

        inGameSplit = null;
        realTimeSplit = null;

        //Establish the real time dates as today, right now.
        currentPlayedRealTime = new DateTime(System.DateTime.Now.Year, System.DateTime.Now.Month, System.DateTime.Now.Day, System.DateTime.Now.Hour, System.DateTime.Now.Minute, System.DateTime.Now.Second);
        lastPlayedRealTime = new DateTime(System.DateTime.Now.Year, System.DateTime.Now.Month, System.DateTime.Now.Day, System.DateTime.Now.Hour, System.DateTime.Now.Minute, System.DateTime.Now.Second);

        //No tool tips have been accessed, so they are wiped.
        ToolTipManager toolTips = FindObjectOfType<ToolTipManager>();
        toolTips.ClearAllTips();

        //There are no unread letters to process.
        unreadLettersProcessed = 0;

        //This clears P&P content.
        pnpVolume = 0;
        pnpChapter = 0;
        pnpPage = 0;
        bookRead = false;

        //The dolls are cleared so no data exists for them.
        //They are checked for existence first to avoid errors.
        if (elizabethAssets != null)
        {
            elizabethAssets.Clear();
        }
        if (janeAssets != null)
        {
            janeAssets.Clear();
        }
        if (elizabethJewelry != null)
        {
            elizabethJewelry.Clear();
        }
        if (janeJewelry != null)
        {
            janeJewelry.Clear();
        }

        //Elizabeth is set as the base doll, and no garments or jewelry have been saved before.
        dollsSaved = false;
        jewelrySaved = false;
        lastAccessedDoll = "Elizabeth";
    }

    //Accesses the save system to load existing player data.
    public void LoadData()
    {
        //First, we get the saved data.
        PlayerData currentData = SaveSystem.LoadData();

        dataExists = true;

        //Load the tableau saved dates.
        tableauDates = new List<DateTime>();
        for (int i = 0; i < currentData.tableauDates.Length; i++)
        {
            //First, break the string up by ints.  Month, Day, Year.
            string[] splitDate = currentData.tableauDates[i].Split("/"[0]);
            tableauDates.Add(new DateTime(int.Parse(splitDate[2]), int.Parse(splitDate[0]), int.Parse(splitDate[1])));
        }

        //Now, we load the saved tableaus.

        //Establish the last time we played this game.
        lastPlayedInGame = SetDate(currentData.gameDatePlayed);
        lastPlayedRealTime = SetDate(currentData.realTimeDatePlayed);

        currentPlayedInGame = SetDate(currentData.gameDatePlayed);
        currentPlayedRealTime = new DateTime(System.DateTime.Now.Year, System.DateTime.Now.Month, System.DateTime.Now.Day, System.DateTime.Now.Hour, System.DateTime.Now.Minute, System.DateTime.Now.Second);

 //       ToolTipManager toolTips = FindObjectOfType<ToolTipManager>();
//        toolTips.AllTipsRead();

        unreadLettersProcessed = currentData.previouslyReadLetters;

        //This updates all of the P&P content.
        pnpVolume = currentData.pnpVol;
		pnpChapter = currentData.pnpChap;
		pnpPage = currentData.pnpPage;
		bookRead = true;
    }
}
