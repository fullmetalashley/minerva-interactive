﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TableauManager : MonoBehaviour
{
    private DataManager playerData;

    private TableauDatabase tableauList;

    public List<Image> tableauImages;

    //These are the locations that the doll will be positioned at.
    public List<Transform> activeDollPositions;
    public List<Transform> inactiveDollPositions;

    public List<string> elizabethKeys;
    public List<string> janeKeys;

    public List<string> elizabethJewelKeys;
    public List<string> janeJewelKeys;

    public List<Image> elizabethClothing;
    public List<Image> janeClothing;

    public List<Image> elizabethJewels;
    public List<Image> janeJewels;

    //These are the images attached to the two dolls that will be filled if sprites exist.
    public Dictionary<string, Image> eDollClothing;
    public Dictionary<string, Image> jDollClothing;

    public Dictionary<string, Image> eDollJewelry;
    public Dictionary<string, Image> jDollJewelry;

    //The navigation pane.
    public GameObject navDrawer;

    //The modal that controls the preview.
    public GameObject previewModal;

    public Image finalTableau;

    public GameObject elizabethDoll;
    public GameObject janeDoll;

    public GameObject imageSavedToast;

    public List<float> dollSizes;


    public int clickedIndex;


    // Start is called before the first frame update
    void Start()
    {
        playerData = FindObjectOfType<DataManager>();
        tableauList = FindObjectOfType<TableauDatabase>();

        eDollClothing = new Dictionary<string, Image>();
        jDollClothing = new Dictionary<string, Image>();

        eDollJewelry = new Dictionary<string, Image>();
        jDollJewelry = new Dictionary<string, Image>();

        for (int i = 0; i < tableauList.activeTableaus.Count; i++)
        {
            tableauImages[i].sprite = tableauList.activeTableaus[i].tableau;
            tableauImages[i].GetComponent<Button>().enabled = true;
            tableauImages[i].GetComponent<Button>().interactable = true;

        }

        for (int i = tableauList.activeTableaus.Count; i < (tableauList.activeTableaus.Count + tableauList.inactiveTableaus.Count); i++)
        {
            tableauImages[i].GetComponent<Button>().enabled = false;
        }
        

        for (int i = 0; i < elizabethKeys.Count; i++)
        {
            eDollClothing.Add(elizabethKeys[i], elizabethClothing[i]);
        }

        for (int i = 0; i < janeKeys.Count; i++)
        {
            jDollClothing.Add(janeKeys[i], janeClothing[i]);
        }

        for (int i = 0; i < elizabethJewelKeys.Count; i++)
        {
            eDollJewelry.Add(elizabethJewelKeys[i], elizabethJewels[i]);
        }

        for (int i = 0; i < janeJewelKeys.Count; i++)
        {
            jDollJewelry.Add(janeJewelKeys[i], janeJewels[i]);
        }

        if (playerData.elizabethAssets != null)
        {
            foreach (KeyValuePair<string, Sprite> eAsset in playerData.elizabethAssets)
            {
                if (eAsset.Value != null)
                {
                    eDollClothing[eAsset.Key].sprite = eAsset.Value;
                    eDollClothing[eAsset.Key].enabled = true;
                }
                else
                {
                    eDollClothing[eAsset.Key].enabled = false;
                }
            }
        }

        if (playerData.janeAssets != null)
        {
            foreach (KeyValuePair<string, Sprite> jAsset in playerData.janeAssets)
            {
                if (jAsset.Value != null)
                {
                    jDollClothing[jAsset.Key].sprite = jAsset.Value;
                    jDollClothing[jAsset.Key].enabled = true;
                }
                else
                {
                    jDollClothing[jAsset.Key].enabled = false;
                }
            }
        }

        if (playerData.elizabethJewelry != null)
        {
            foreach (KeyValuePair<string, Sprite> eJewel in playerData.elizabethJewelry)
            {
                if (eJewel.Value != null)
                {
                    eDollJewelry[eJewel.Key].sprite = eJewel.Value;
                    eDollJewelry[eJewel.Key].enabled = true;
                }
                else
                {
                    eDollJewelry[eJewel.Key].enabled = false;
                }
            }
        }

        if (playerData.janeJewelry != null)
        {
            foreach (KeyValuePair<string, Sprite> jJewel in playerData.janeJewelry)
            {
                if (jJewel.Value != null)
                {
                    jDollJewelry[jJewel.Key].sprite = jJewel.Value;
                    jDollJewelry[jJewel.Key].enabled = true;
                }
                else
                {
                    jDollJewelry[jJewel.Key].enabled = false;
                }
            }
        }
    }

    public void PopulateClothes()
    {

    }


    //Used by the tableau buttons.
    public void TogglePreviewModal(int index)
    {
        navDrawer.SetActive(!navDrawer.activeSelf);
        finalTableau.sprite = tableauImages[index].sprite;

        previewModal.SetActive(!previewModal.activeSelf);

        //Set the doll positions.
        elizabethDoll.transform.position = activeDollPositions[index].position;
        janeDoll.transform.position = inactiveDollPositions[index].position;

        if (previewModal.activeSelf)
        {
            elizabethDoll.transform.localScale *= dollSizes[index];
            janeDoll.transform.localScale *= dollSizes[index];
            clickedIndex = index;

        }
        else
        {
            elizabethDoll.transform.localScale *= 1f / dollSizes[index];
            janeDoll.transform.localScale *= 1f / dollSizes[index];
        }
    }

    //Used by the back button, same function but with no index.
    public void TogglePreviewModal()
    {
        navDrawer.SetActive(!navDrawer.activeSelf);

        previewModal.SetActive(!previewModal.activeSelf);

        if (previewModal.activeSelf)
        {
            elizabethDoll.transform.localScale *= dollSizes[clickedIndex];
            janeDoll.transform.localScale *= dollSizes[clickedIndex];
        }
        else
        {
            elizabethDoll.transform.localScale *= 1f / dollSizes[clickedIndex];
            janeDoll.transform.localScale *= 1f / dollSizes[clickedIndex];
        }
    }

    public void ImageSaved()
    {
        if (!imageSavedToast.GetComponent<MiniToolTipControl>().running){
            imageSavedToast.GetComponent<MiniToolTipControl>().StartAnimation();

        }
    }
}
