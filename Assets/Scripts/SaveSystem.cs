﻿using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public class SaveSystem : MonoBehaviour
{
    private DataManager inGameData;


    // Start is called before the first frame update
    void Start()
    {
        inGameData = GetComponent<DataManager>();
    }

    private void Awake()
    {
    }

    //This will save the game when the player has closed the app.
    public void OnApplicationQuit()
    {
        inGameData.SaveGame();
    }

    //This will enact a process when the player has suspended the app.
    public void OnApplicationPause(bool pause)
    {
    }

    //This saves the player data to a .tiv file.  
    public void SaveData()
    {
        BinaryFormatter formatter = new BinaryFormatter();
        string path = Application.persistentDataPath + "/player.tiv";
        FileStream stream = new FileStream(path, FileMode.Create);

        //Writing to file
        PlayerData data = new PlayerData(inGameData.playerName, inGameData.inGameSplit, inGameData.realTimeSplit, inGameData.pnpVolume, 
            inGameData.pnpChapter, inGameData.pnpPage, inGameData.unreadLettersProcessed, inGameData.debugDatesActive, inGameData.tableauDatesString);

        formatter.Serialize(stream, data);
        stream.Close();
    }

    //This loads the existing player data.
    public static PlayerData LoadData()
    {
        string path = Application.persistentDataPath + "/player.tiv";
        if (File.Exists(path))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(path, FileMode.Open);

            PlayerData data = formatter.Deserialize(stream) as PlayerData;
            stream.Close();

            return data;
        }
        else
        {
            Debug.Log("Save file not found in " + path);
            return null;
        }
    }
    
    //A method checking to see if player data exists or not.
    public bool PlayerDataExists()
    {
        string path = Application.persistentDataPath + "/player.tiv";
        if (File.Exists(path))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    //A player's file can be deleted from within the game.
    //If we keep this, and it is used, the game should also perform a reset to the start with wiped content.
    public void ClearData()
    {
        string path = Application.persistentDataPath + "/player.tiv";
        if (File.Exists(path))
        {
            File.Delete(Application.persistentDataPath + "/player.tiv");
            Debug.Log("File deleted.");
        }
        else
        {
            Debug.Log("No file found in " + path);
        }

        
        string[] fileNames = Directory.GetFiles(Application.persistentDataPath);
        for (int i = 0; i < fileNames.Length; i++)
        {
            if (fileNames[i].Contains("Tableau"))
            {
                File.Delete(fileNames[i]);
            }
        }
    }
}
