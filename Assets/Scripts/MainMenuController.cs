﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuController : MonoBehaviour
{
    public GameObject menuModal;
    public GameObject creditsScene;


    //Turn the main menu on and off.
    public void ToggleModal()
    {
        menuModal.SetActive(!menuModal.activeSelf);
    }

    //Turn the credits panel on and off.
    public void Credits()
    {
        creditsScene.SetActive(!creditsScene.activeSelf);
    }
}
