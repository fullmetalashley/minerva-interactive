﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PnPController : MonoBehaviour
{
    private PnPDatabase pnpNovel;
    private DataManager playerData;

    public int pnpIndex;

    public GameObject pnp;

    public GameObject chapterHeading;

    public GameObject scrollRight;
    public GameObject scrollLeft;

    public bool outOfContent;

    public Text pnpContentHalf;
    public Text pnpContentFull;
    public Text pnpVolume;
    public Text pnpChapter;
    public Text pnpPage;

    public int pnpVolIndex = 0;
    public int pnpChapIndex = 0;
    public int pnpPageIndex = 0;

    public int totalVolumes;
    public int totalChapters;
    public int chap1Pages;
    public int chap2Pages;
    public int chap3Pages;
    public int chap4Pages;
    public int chap5Pages;
    public int chap6Pages;
    public int chap7Pages;

    public int volumes;
    public int chapters;
    public int pages;

    // Start is called before the first frame update
    void Start()
    {
        pnpNovel = FindObjectOfType<PnPDatabase>();
        playerData = FindObjectOfType<DataManager>();

        totalVolumes = pnpNovel.volume1.Count;
        totalChapters = pnpNovel.volume1[1].Count;

        chap1Pages = pnpNovel.volume1[1][0].Count;
        chap2Pages = pnpNovel.volume1[1][1].Count;
        chap3Pages = pnpNovel.volume1[1][2].Count;
        chap4Pages = pnpNovel.volume1[1][3].Count;
        chap5Pages = pnpNovel.volume1[1][4].Count;
        chap6Pages = pnpNovel.volume1[1][5].Count;
        chap7Pages = pnpNovel.volume1[1][6].Count;

        volumes = pnpNovel.volume1.Count - 1;   //This goes by index, not length of the list.
        chapters = pnpNovel.volume1[pnpNovel.volume1.Count].Count;
        pages = pnpNovel.volume1[pnpNovel.volume1.Count][chapters - 1].Count;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void PageCheck()
    {
        //We need to check the other end as well.


        if (pnpChapIndex == 0 && pnpVolIndex == 0 && pnpPageIndex == 0)
        {
            scrollLeft.SetActive(false);
        }

        else if (((pnpChapIndex + 1) == chapters) && pnpVolIndex == volumes && ((pnpPageIndex + 1) == pages))
        {
            scrollRight.SetActive(false);
        }
        else
        {
            scrollRight.SetActive(true);
            scrollLeft.SetActive(true);
        }
    }

    public void ReversePagePnP()
    {
        
        //First, have we officially hit the bottom?
        if (!outOfContent)
        {
            scrollRight.SetActive(true);
            //Okay, there is at least a tiny bit left.  Great.  So let's start from the top.  Take away a page.
            pnpPageIndex--;
            //Now, we need to know if we are at the end of a chapter.  If the page index is -1, we have passed the chapter.
            if (pnpPageIndex == -1)
            {
                //We have passed the chapter.  So we should move to the next one and reset the page index.
                pnpChapIndex--;
                //Are we at the end of the volume?
                if (pnpChapIndex == -1)
                {
                    //We've used all the chapters.  
                    pnpVolIndex--;
                    if (pnpVolIndex == -1)
                    {
                        //That's it, that's all the content.
                        Debug.Log("100% out of content!");
                        //Set every index to 0.
                        pnpPageIndex = 0;
                        pnpChapIndex = 0;
                        pnpVolIndex = 0;
                        outOfContent = true;
                        scrollLeft.SetActive(false);
                    }
                }
                else
                {
                    Debug.Log("We have changed chapters!");
                    //We aren't at the end of the volume, so set the page index to the end of this chapter.
                    pnpPageIndex = pnpNovel.volume1[pnpVolIndex + 1][pnpChapIndex].Count - 1;
                }
            }
        }
        //We are not at the end of a chapter, we're okay.  This is just the next page.
        //Set both text blocks.
        pnpContentFull.text = pnpNovel.volume1[pnpVolIndex + 1][pnpChapIndex][pnpPageIndex];
        pnpContentHalf.text = pnpNovel.volume1[pnpVolIndex + 1][pnpChapIndex][pnpPageIndex];
        if (pnpPageIndex == 0)
        {
            //We are at the end of a new chapter, so we need to use the half block.
            pnpContentFull.enabled = false;
            pnpContentHalf.enabled = true;
            pnpChapter.enabled = true;
            chapterHeading.SetActive(true);
            pnpVolume.enabled = true;
        }
        else
        {
            chapterHeading.SetActive(false);
            pnpContentFull.enabled = true;
            pnpChapter.enabled = false;
            pnpContentHalf.enabled = false;
            pnpVolume.enabled = false;
        }


        pnpPage.text = "Page " + (pnpPageIndex + 1);
        pnpChapter.text = "Chapter " + (pnpChapIndex + 1);
        pnpVolume.text = "Volume " + (pnpVolIndex + 1);

        playerData.pnpChapter = pnpChapIndex;
        playerData.pnpPage = pnpPageIndex;
        playerData.pnpVolume = pnpVolIndex;
        PageCheck();
    }
    


    public void ChangePagePnP()
    {
        outOfContent = false;
        pnpPageIndex++;
        scrollLeft.SetActive(true);
        if (pnpVolIndex < pnpNovel.volume1.Count) //If the volume index is less than the dictionary length, we are still in the text.
        {
            if (pnpPageIndex < pnpNovel.volume1[pnpVolIndex + 1][pnpChapIndex].Count) //If the current page is less than the length of the current chapter...
            {
                pnpContentFull.enabled = true;
                pnpContentFull.text = pnpNovel.volume1[pnpVolIndex + 1][pnpChapIndex][pnpPageIndex];
                pnpContentHalf.enabled = false;
                pnpPage.text = "Page " + (pnpPageIndex + 1);
            }
            else
            {
                //We need to change chapters.  //This means we should start the next amount of content on a half page.
                pnpChapIndex++;
                pnpPageIndex = 0;
                if (!(pnpChapIndex < pnpNovel.volume1[pnpVolIndex + 1].Count))
                {
                    //We are no longer within the limits of this volume.
                    pnpVolIndex++;
                    if (!(pnpVolIndex < pnpNovel.volume1.Count))
                    {
                        Debug.Log("We are out of volumes!");
                        scrollRight.SetActive(false);
                    }
                    else
                    {
                        //We are in the next volume and can update accordingly.
                        pnpContentHalf.text = pnpNovel.volume1[pnpVolIndex + 1][pnpChapIndex][pnpPageIndex];
                        pnpContentHalf.enabled = true;
                        pnpContentFull.enabled = false;
                        pnpPage.text = "Page " + (pnpPageIndex + 1);
                        pnpChapter.text = "Chapter " + (pnpChapIndex + 1);
                        pnpVolume.text = "Volume " + (pnpVolIndex + 1);
                    }
                }
                else
                {
                    //We are in the next chapter and can update accordingly.
                    pnpContentHalf.text = pnpNovel.volume1[pnpVolIndex + 1][pnpChapIndex][pnpPageIndex];
                    pnpContentHalf.enabled = true;
                    pnpContentFull.enabled = false;
                    pnpPage.text = "Page " + (pnpPageIndex + 1);
                    pnpChapter.text = "Chapter " + (pnpChapIndex + 1);
                }
            }

            //Disable chapter content as needed.
            if (pnpPageIndex == 0)
            {
                pnpChapter.enabled = true;
                pnpVolume.enabled = true;
                chapterHeading.SetActive(true);

            }
            else
            {
                pnpChapter.enabled = false;
                pnpVolume.enabled = false;
                chapterHeading.SetActive(false);
            }
            //Set the player data to the current values.
            playerData.pnpChapter = pnpChapIndex;
            playerData.pnpPage = pnpPageIndex;
            playerData.pnpVolume = pnpVolIndex;
        }
        else
        {
            pnpContentHalf.text = "I'm sorry, there's no more content to read!";
        }
        PageCheck();
    }

    public void ClosePnP()
    {
        Screen.orientation = ScreenOrientation.LandscapeLeft;
        pnp.SetActive(false);
        playerData.pnpChapter = pnpChapIndex;
        playerData.pnpPage = pnpPageIndex;
        playerData.pnpVolume = pnpVolIndex;
    }

    public void OpenPnP()
    {
        //Set screen orientation.
        Screen.orientation = ScreenOrientation.Portrait;

        if (playerData.bookRead)
        {
            pnpChapIndex = playerData.pnpChapter;
            pnpPageIndex = playerData.pnpPage;
            pnpVolIndex = playerData.pnpVolume;
            Debug.Log("We read the book and loaded it!");
        }
        pnp.SetActive(true);
        //How do we know if it's the start of a chapter or not?
        if (pnpPageIndex == 0)
        {
            pnpContentHalf.text = pnpNovel.volume1[pnpVolIndex + 1][pnpChapIndex][pnpPageIndex];
            pnpContentHalf.enabled = true;
            pnpContentFull.enabled = false;

            chapterHeading.SetActive(true);
            pnpChapter.enabled = true;
            pnpVolume.enabled = true;
        }
        else
        {
            pnpContentFull.text = pnpNovel.volume1[pnpVolIndex + 1][pnpChapIndex][pnpPageIndex];
            pnpContentFull.enabled = true;
            pnpContentHalf.enabled = false;

            

            chapterHeading.SetActive(false);
            pnpChapter.enabled = false;
            pnpVolume.enabled = false;
        }




        pnpVolume.text = "Volume " + (pnpVolIndex + 1);
        pnpChapter.text = "Chapter " + (pnpChapIndex + 1);
        pnpPage.text = "Page " + (pnpPageIndex + 1);
    }
}
