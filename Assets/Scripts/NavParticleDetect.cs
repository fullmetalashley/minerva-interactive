﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NavParticleDetect : MonoBehaviour
{
    private DataManager playerData;
    // Start is called before the first frame update
    void Start()
    {
        playerData = FindObjectOfType<DataManager>();
        if (playerData.navGlowEncountered)
        {
            this.gameObject.SetActive(false);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
