﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class MailController : MonoBehaviour
{

    public MailDatabase mailLists;
    public int currentIndex;

    public Text sender;
    public Text from;
    public Text content;

    public GameObject letter;

    public Image nextButton;

    public GameObject unreadMailIcon;

    public GameObject readMailModal;

    public GameObject unrealLetterBuffer;

    public GameObject mainMenuButton;

    public List<ReadLetterPanel> readLetterList;

    public bool readLetterOn;

    public Image unreadLetterBundle;

    public Sprite unreadMailOpen;
    public Sprite unrealMailClosed;



    public List<Image> deliveredItems;
    public GameObject deliveryModal;

    private DataManager playerData;

    public GameObject closeButton;

    public GameObject readLetterBuffer;


    // Start is called before the first frame update
    void Start()
    {
        mailLists = FindObjectOfType<MailDatabase>();
        playerData = FindObjectOfType<DataManager>();

        //Check to see if there is any unread mail in the database.  If so, set the icon to active.
        UnreadIndicator();
    }

    public void UnreadIndicator()
    {
        if (mailLists.unreadMail.Count > 0)
        {
            unreadMailIcon.SetActive(true);
        }
        else
        {
            unreadMailIcon.SetActive(false);
        }
    }

    //Turns the mail modal on and off.
    //Called by the read mail bundle within the writing desk modal.
    public void ToggleReadMailModal()
    {
        readMailModal.SetActive(!readMailModal.activeSelf);
        mailLists.readMail.Reverse();   //Reverses the order of the mail in the list so that it goes from newest to oldest.
        PopulateReadLetter();   //Sets all of the items in the modal to active or inactive based on whether or not they have a corresponding letter.
    }

    //This is used by the letter blocker to disable a letter when it is active.
    public void ToggleLetter()
    {
        currentIndex = 0;
        nextButton.enabled = true;
        letter.SetActive(!letter.activeSelf);
        if (letter.activeSelf)
        {
            readLetterBuffer.SetActive(false);
            if (mailLists.unreadMail.Count > 0)
            {
                PopulateUnreadLetter();
            }
        }
        else
        {
            unrealLetterBuffer.SetActive(false);
        }
    }
    
    //This is called by the writing desk hotspot as soon as it is clicked to toggle the entire modal.
    public void InitialLetterDisplay()
    {
        currentIndex = 0;
        readLetterBuffer.SetActive(false);
        if (mailLists.unreadMail.Count > 0)
        {
            letter.SetActive(true);
            unrealLetterBuffer.SetActive(true);
            PopulateUnreadLetter();
            if (mailLists.unreadMail.Count != 1)
            {
                nextButton.enabled = true;
            }
            else
            {
                nextButton.enabled = false;
            }
        }
        
    }

    //The modal list of letters is parsed to set up the scrollable items with their corresponding letters.
    public void PopulateReadLetter() {
        for (int i = 0; i < mailLists.readMail.Count; i++)
        {
            readLetterList[i].gameObject.SetActive(true);
            readLetterList[i].sender.text = mailLists.readMail[i].sender;
            readLetterList[i].dateSent.text = mailLists.readMail[i].sentDate;
        }

        for (int m = mailLists.readMail.Count; m < readLetterList.Count; m++)
        {
            readLetterList[m].gameObject.SetActive(false);
        }
    }

    public void PopulateUnreadLetter()
    {
        if (mailLists.unreadMail[currentIndex].hasItem)
        {
            deliveryModal.SetActive(true);
            for (int i = 0; i < mailLists.unreadMail[currentIndex].deliveredImages.Length; i++)
            {
                deliveredItems[i].enabled = true;

                deliveredItems[i].sprite = mailLists.unreadMail[currentIndex].deliveredImages[i];
            }
            for (int p = mailLists.unreadMail[currentIndex].deliveredImages.Length; p < deliveredItems.Count; p++)

            {
                deliveredItems[p].enabled = false;
            }
        }
        else
        {
            deliveryModal.SetActive(false);
        }
        sender.text = mailLists.unreadMail[currentIndex].sender;
        from.text = mailLists.unreadMail[currentIndex].sentDate;
        content.text = mailLists.unreadMail[currentIndex].content;

        mailLists.readMail.Add(mailLists.unreadMail[currentIndex]);
    }

    public void DisplayReadLetter(int index)
    {
        readLetterOn = true;

        letter.SetActive(true);
        unrealLetterBuffer.SetActive(true);

        sender.text = mailLists.readMail[index].sender;
        from.text = mailLists.readMail[index].sentDate;
        content.text = mailLists.readMail[index].content;

        nextButton.enabled = false;
        readLetterBuffer.SetActive(true);
        if (mailLists.readMail[index].hasItem)
        {
            deliveryModal.SetActive(true);
            for (int i = 0; i < mailLists.readMail[index].deliveredImages.Length; i++)
            {
                deliveredItems[i].enabled = true;

                deliveredItems[i].sprite = mailLists.readMail[index].deliveredImages[i];
            }
            for (int p = mailLists.readMail[index].deliveredImages.Length; p < deliveredItems.Count; p++)

            {
                deliveredItems[p].enabled = false;
            }
        }
        else
        {
            deliveryModal.SetActive(false);
        }
    }


    public void NextLetter()
    {
        if (readLetterOn)
        {
            letter.SetActive(false);
            unrealLetterBuffer.SetActive(false);
            readLetterOn = false;
        }
        else
        {
            //We are still reading new mail.
            if (mailLists.unreadMail.Count > 0)
            {
                playerData.unreadLettersProcessed++;
                currentIndex++;

                if (currentIndex == (mailLists.unreadMail.Count - 1))
                {
                    nextButton.enabled = false;
                    readLetterBuffer.SetActive(true);

                }
                if (currentIndex < mailLists.unreadMail.Count)
                {
                    PopulateUnreadLetter();
                }
                else
                {
                    letter.SetActive(false);
                    unrealLetterBuffer.SetActive(false);
                    mailLists.MoveUnreadLetters();

                    unreadLetterBundle.sprite = unreadMailOpen;
                }
            }
        }
    }
}
