﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;

public class PassiveElementManager : MonoBehaviour
{
    //All of the sprites for the environment that will change based on the time of day.
    public List<Sprite> backdropAssets;
    public List<Sprite> jewelryAssets;
    public List<Sprite> wardrobeAssets;
    public List<Sprite> windowBenchAssets;
    public List<Sprite> writingAssets;

    //The strings that reference the times of day that will help establish the dictionaries.
    public List<String> stateChanges;

    //The image references for the objects that will change their sprites multiple times.
    public Image backdrop;
    public Image jewelry;
    public Image wardrobe;
    public Image windowBench;
    public Image writing;

    //The dictionaries that will store the sprites and strings accordingly.
    public Dictionary<string, Sprite> backdropChanges;
    public Dictionary<string, Sprite> jewelryChanges;
    public Dictionary<string, Sprite> wardrobeChanges;
    public Dictionary<string, Sprite> windowBenchChanges;
    public Dictionary<string, Sprite> writingChanges;

    private DataManager playerData;

    //Changes the main owl icon based on the time of day.
    public Image mainMenuOwl;
    public Sprite dayOwl;
    public Sprite nightOwl;


    // Start is called before the first frame update
    void Start()
    {
        playerData = FindObjectOfType<DataManager>();

        Debug.Log("CURRENT GAME DATE: " + playerData.currentPlayedInGame.Month + "/" + playerData.currentPlayedInGame.Day + "/" + playerData.currentPlayedInGame.Year);

        backdropChanges = new Dictionary<string, Sprite>();
        jewelryChanges = new Dictionary<string, Sprite>();
        wardrobeChanges = new Dictionary<string, Sprite>();
        windowBenchChanges = new Dictionary<string, Sprite>();
        writingChanges = new Dictionary<string, Sprite>();

        //Establish the dictionaries for each set of images.
        for (int i = 0; i < stateChanges.Count; i++)
        {
            backdropChanges.Add(stateChanges[i], backdropAssets[i]);
            jewelryChanges.Add(stateChanges[i], jewelryAssets[i]);
            wardrobeChanges.Add(stateChanges[i], wardrobeAssets[i]);
            windowBenchChanges.Add(stateChanges[i], windowBenchAssets[i]);
            writingChanges.Add(stateChanges[i], writingAssets[i]);
        }
    }

    public void UpdatePassives()
    {
        int newHour = System.DateTime.Now.Hour;
        //If it is between 6 AM and 12 PM, set to morning time.
        //If it is between 6 PM and 6 AM, set to nighttime.
        if (6 < newHour && newHour < 18)
        {
            windowBench.sprite = windowBenchChanges["Afternoon"];
            jewelry.sprite = jewelryChanges["Afternoon"];
            writing.sprite = writingChanges["Afternoon"];
            wardrobe.sprite = wardrobeChanges["Afternoon"];
            backdrop.sprite = backdropChanges["Afternoon"];
            mainMenuOwl.sprite = dayOwl;
        }
        else {
            windowBench.sprite = windowBenchChanges["Night"];
            jewelry.sprite = jewelryChanges["Night"];
            writing.sprite = writingChanges["Night"];
            wardrobe.sprite = wardrobeChanges["Night"];
            backdrop.sprite = backdropChanges["Night"];
            mainMenuOwl.sprite = nightOwl;
        }
    }
}
