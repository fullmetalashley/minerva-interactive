﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PnPDatabase : MonoBehaviour
{
    public static PnPDatabase control;

    public Dictionary<int, List<List<string>>> volume1;

    void Awake()
    {
        if (control == null)
        {
            DontDestroyOnLoad(gameObject);
            control = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }
}
