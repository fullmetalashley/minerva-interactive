﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuitGame : MonoBehaviour
{
    //The application will exit.  Called from the Start Screen and the main menu button.
    public void Quit()
    {
        Application.Quit();
    }
}
