﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PlayerData 
{
    //This is the class that stores all of the player's necessary data between sessions.
    //Name is not necessary and is purely for testing purposes right now.
    public string playerName;
    //This is a broken int array of the last date played, and is translated into a DateTime.  THIS IS THE IN GAME DATE.
    public int[] gameDatePlayed;
    //This shows the last page the player was reading.  Will need to be expanded to contain volumes / chapters eventually.
    public int pnpIndex;
    //This saves the last time the player accessed the app in real time.
    public int[] realTimeDatePlayed;

    //These are the values that record where the player last read the book.
    public int pnpVol;
    public int pnpChap;
    public int pnpPage;
    public int previouslyReadLetters;

    public bool debuggingActive;

    public string[] tableauDates;

    public PlayerData(string value)
    {
        playerName = value;
    }

    public PlayerData(string value, int[] date)
    {
        playerName = value;
        gameDatePlayed = date;
    }

    //This is how the constructor will be ideally.  
    public PlayerData(string value, int[] date, int[] realTimeDate)
    {
        playerName = value;
        gameDatePlayed = date;
        realTimeDatePlayed = realTimeDate;
    }

    //This constructor contains book data and saved tableaus.
    public PlayerData(string value, int[] date, int[] realTimeDate, int pnpVol, int pnpChap, int pnpPage, int readLetters, bool debuggingActive, string[] tableauDates)
    {
        playerName = value;
        gameDatePlayed = date;
        realTimeDatePlayed = realTimeDate;
        this.pnpVol = pnpVol;
        this.pnpChap = pnpChap;
        this.pnpPage = pnpPage;
        previouslyReadLetters = readLetters;

        this.debuggingActive = debuggingActive;
        this.tableauDates = tableauDates;
    }
}
