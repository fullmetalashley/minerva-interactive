﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class DebugClock : MonoBehaviour
{

    public Text systemTime;
    public Text systemDate;
    public Text inGameDate;
    public Text lastDayPlayedRealTime;
    public Text lastHourPlayedRealTime;

    public Text currentAct;

    public Text dateMessage;

    private int hours;
    private int seconds;
    private int minutes;

    private int month;
    private int day;
    private int year;

    private TimeCheck runningClock;
    private DataManager playerData;
    private ClockTracker mainClock;


    // Start is called before the first frame update
    void Start()
    {
        playerData = FindObjectOfType<DataManager>();
        mainClock = FindObjectOfType<ClockTracker>();
        //We can get the time from the player data or the main clock.  Those will be present on every scene.

        UpdateText();
    }

    public void UpdateText()
    {
        systemDate.text = "System date: " + playerData.currentPlayedRealTime.Month + "/" + playerData.currentPlayedRealTime.Day + "/" + playerData.currentPlayedRealTime.Year;

        //Minutes don't always display if it's a single digit, so adding the 0 here helps for display purposes.
        if (playerData.currentPlayedRealTime.Minute < 10)
        {
            systemTime.text = "System time: " + playerData.currentPlayedRealTime.Hour + ":0" + playerData.currentPlayedRealTime.Minute;

        }
        else
        {
            systemTime.text = "System time: " + playerData.currentPlayedRealTime.Hour + ":" + playerData.currentPlayedRealTime.Minute;
        }

        inGameDate.text = "Current in-game day: " + playerData.currentPlayedInGame.Month + "/" + playerData.currentPlayedInGame.Day + "/" + playerData.currentPlayedInGame.Year;
        currentAct.text = "Current act: " + (mainClock.currentActIndex + 1);
        lastDayPlayedRealTime.text = "Last Session: " + playerData.lastPlayedRealTime.Month + "/" + playerData.lastPlayedRealTime.Day + "/" + playerData.lastPlayedRealTime.Year;
        lastHourPlayedRealTime.text = "Last Session Time: " + playerData.lastPlayedRealTime.Hour + ":" + playerData.lastPlayedRealTime.Minute;

        dateMessage.text = "It has been " + mainClock.DebuggingTimeDiff(playerData.currentPlayedRealTime, playerData.lastPlayedRealTime) + " days since you last played!";

    }

    public void ResetGameDay()
    {
        mainClock.ResetAtBeginning();
        playerData.currentPlayedRealTime = new DateTime(System.DateTime.Now.Year, System.DateTime.Now.Month, System.DateTime.Now.Day, System.DateTime.Now.Hour, System.DateTime.Now.Minute, System.DateTime.Now.Second);
        UpdateText();
    }

    public void ClockUpdate()
    {
        mainClock.CalculateTimeDifference(playerData.currentPlayedRealTime, playerData.lastPlayedRealTime);
        UpdateText();
    }

    public void AdvanceGameDay()
    {
        mainClock.AdvanceGameDay();
        UpdateText();
    }

    public void AdvanceCurrentAct()
    {
        mainClock.StartNextAct();
        UpdateText();
    }

    public void OnCloseUpdate()
    {
        PassiveElementManager thePassive = FindObjectOfType<PassiveElementManager>();
        thePassive.UpdatePassives();
    }

    public void AddHours()
    {
        playerData.currentPlayedRealTime = playerData.currentPlayedRealTime.AddHours(1.0);
        UpdateText();
    }

    public void SubtractHours()
    {
        playerData.currentPlayedRealTime = playerData.currentPlayedRealTime.AddHours(-1.0);
        UpdateText();
    }

    public void AddYear()
    {
        playerData.currentPlayedRealTime = playerData.currentPlayedRealTime.AddYears(1);
        UpdateText();
    }

    public void SubtractYear()
    {
        playerData.currentPlayedRealTime = playerData.currentPlayedRealTime.AddYears(-1);
        UpdateText();
    }

    public void AddMonth()
    {
        playerData.currentPlayedRealTime = playerData.currentPlayedRealTime.AddMonths(1);
        UpdateText();
    }

    public void SubtractMonth()
    {
        playerData.currentPlayedRealTime = playerData.currentPlayedRealTime.AddMonths(-1);
        UpdateText();
    }

    public void AddDay()
    {
        playerData.currentPlayedRealTime = playerData.currentPlayedRealTime.AddDays(1.0);
        UpdateText();
    }

    public void SubtractDay()
    {
        playerData.currentPlayedRealTime = playerData.currentPlayedRealTime.AddDays(-1.0);
        UpdateText();
    }

    public void AddHoursPrevSave()
    {
        playerData.lastPlayedRealTime = playerData.lastPlayedRealTime.AddHours(1.0);
        UpdateText();
    }

    public void SubtractHoursPrevSave()
    {
        playerData.lastPlayedRealTime = playerData.lastPlayedRealTime.AddHours(-1.0);
        UpdateText();
    }

    public void AddYearPrevSave()
    {
        playerData.lastPlayedRealTime = playerData.lastPlayedRealTime.AddYears(1);
        UpdateText();
    }

    public void SubtractYearPrevSave()
    {
        playerData.lastPlayedRealTime = playerData.lastPlayedRealTime.AddYears(-1);
        UpdateText();
    }

    public void AddMonthPrevSave()
    {
        playerData.lastPlayedRealTime = playerData.lastPlayedRealTime.AddMonths(1);
        UpdateText();
    }

    public void SubtractMonthPrevSave()
    {
        playerData.lastPlayedRealTime = playerData.lastPlayedRealTime.AddMonths(-1);
        UpdateText();
    }

    public void AddDayPrevSave()
    {
        playerData.lastPlayedRealTime = playerData.lastPlayedRealTime.AddDays(1.0);
        UpdateText();
    }

    public void SubtractDayPrevSave()
    {
        playerData.lastPlayedRealTime = playerData.lastPlayedRealTime.AddDays(-1.0);
        UpdateText();
    }

    public void ReloadGame()
    {
        //Force a reload of the entire game using the CURRENT REAL TIME as the simulated real time, and the CURRENT LAST OPEN TIME as the simulated prev save.
    }
}
