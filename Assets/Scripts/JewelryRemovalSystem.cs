﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class JewelryRemovalSystem : MonoBehaviour
{
    public GameObject janeRemoval;
    public GameObject elizabethRemoval;

    public JewelryDollManager jewelDolls;

    public Button removalButton;



    // Start is called before the first frame update
    void Start()
    {
        jewelDolls = FindObjectOfType<JewelryDollManager>();

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public bool CheckForClothing()
    {
        jewelDolls = FindObjectOfType<JewelryDollManager>();
        if (jewelDolls.currentDoll.name == "Elizabeth")
        {
            foreach (KeyValuePair<string, ClothingSlot> slots in jewelDolls.Elizabeth.dollSlots)
            {
                if (slots.Value.hasClothing)
                {
                    //There is at least one piece of clothing in here, so we can return true.
                    return true;
                }
            }
            return false;
        }
        else
        {
            foreach (KeyValuePair<string, ClothingSlot> slots in jewelDolls.Jane.dollSlots)
            {
                if (slots.Value.hasClothing)
                {
                    //There is at least one piece of clothing in here, so we can return true.
                    return true;
                }
            }
            return false;
        }
        
    }

    public void RemovePiece(string pieceToRemove)
    {
        jewelDolls.RemoveSpecificArticle(pieceToRemove);
        //If all pieces of jewelry are gone, automatically turn off removal system.
       if (!CheckForClothing())
        {
            Debug.Log("ToggleRemoval!");
            ToggleRemoval();
        }
        else
        {
            Debug.Log("Nope clothes are still on.");
        }
    }

    public void ToggleRemoval()
    {
        if (jewelDolls.currentDoll.name == "Jane")
        {
            janeRemoval.SetActive(!janeRemoval.activeSelf);
        }
        else
        {
            elizabethRemoval.SetActive(!elizabethRemoval.activeSelf);
        }

        if (CheckForClothing())
        {
            removalButton.interactable = true;
        }
        else
        {
            removalButton.interactable = false;
            RemovalOff();
        }
    }

    public void RemovalOff()
    {
        janeRemoval.SetActive(false);
        elizabethRemoval.SetActive(false);
    }
}
