﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{
    //Set this string in the Unity Editor.
    public string SceneToLoad;

    public void LoadScene()
    {
        //Set a reference to the player data.  Will not be permanent; will change depending on the scene.
        DataManager playerData = FindObjectOfType<DataManager>();

        //Establish the last scene in the Player Data as this current scene.
        playerData.lastScene = SceneManager.GetActiveScene().name;
        SceneManager.LoadScene(SceneToLoad);
    }
}
