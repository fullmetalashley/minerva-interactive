﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Doll 
{
    //These sprites store the doll's clothing and body base to be loaded onto the active / inactive dolls as necessary.
    public Sprite bodyBase;
    public Sprite dress;
    public Sprite shoes;
    public Sprite belt;

    public string name;

    public Dictionary<string, Sprite> articlesOfClothing;

    public Doll()
    {

    }

    //The constructor takes in a body base and string for the doll's name.  The doll's name is likely unnecessary, but is stored in case we utilize this in the future.
    public Doll (Sprite bodyBase, string name)
    {
        this.bodyBase = bodyBase;
        this.name = name;
        articlesOfClothing = new Dictionary<string, Sprite>();

        articlesOfClothing.Add("Dresses", dress);
        articlesOfClothing.Add("Shoes", shoes);
        articlesOfClothing.Add("Belts", belt);

        
    }
}
