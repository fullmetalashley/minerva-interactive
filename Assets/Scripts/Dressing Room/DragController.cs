﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class DragController : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    //This is the object that will be dragged.  It's static so only one object can be dragged at a time.
    public static GameObject itemBeingDragged;
    Vector3 startPosition;

    //The starting position of the dragging object.
    Transform startParent;
    //The canvas is used as the main area that the object can't be dragged out of.  It becomes the parent when the object is dragged out of the scroll window.
    public GameObject canvas;

    private ClosetManager theCloset;
    private DollManager theDoll;
    private CanvasGroup cGroup;
    private ClothingPopulator clothesManager;

    private HitboxController hitboxes;




    //Index is set manually in the Inspector, corresponding to it's point on the scrolling grid.
    public int index;

    // Start is called before the first frame update
    void Start()
    {
        hitboxes = FindObjectOfType<HitboxController>();
        theCloset = FindObjectOfType<ClosetManager>();
        theDoll = FindObjectOfType<DollManager>();
        clothesManager = FindObjectOfType<ClothingPopulator>();
        //A canvas group needs to be attached to each draggable object.
        cGroup = GetComponent<CanvasGroup>();
    }

    //The following are the interface implementations to allow us to use those interfaces.
    public void OnBeginDrag(PointerEventData eventData)
    {
        itemBeingDragged = gameObject;
        startPosition = transform.position;
        startParent = transform.parent;
        //Block the raycasts of the canvas group this is a part of in order to prevent it from hiding detections.
        cGroup.blocksRaycasts = false;


        //We're dragging something from the combined outerwear category.
        if (clothesManager.outerwearOn)
        {
            string outerwear = "";
            //If we are between 0 and the jacket index, it's a jacket.
            if (0 <= index && index <= clothesManager.lastJacketIndex)
            {
                clothesManager.currentCategory = "Jackets";
                outerwear = "Jackets";
            }else if (clothesManager.lastJacketIndex < index && index <= clothesManager.lastShawlIndex)
            {
                //If we are between jacket index and shawl index, it's a shawl.
                clothesManager.currentCategory = "Shawls";
                outerwear = "Shawls";
            }
            else if (clothesManager.lastShawlIndex < index && index <= clothesManager.lastCapeIndex)
            {
                //It's a cape.
                clothesManager.currentCategory = "Capes";
                outerwear = "Capes";
            }

            clothesManager.OuterwearSwapLists(outerwear);
            theCloset.NewSlotActive();
        }

        if (clothesManager.currentCategory == "Gloves")
        {
            theDoll.currentDoll.dollClothing["Left Glove"].raycastTarget = true;
            theDoll.currentDoll.dollClothing["Right Glove"].raycastTarget = true;

            foreach(KeyValuePair<string, Image> img2 in theDoll.currentDoll.dollClothing)
            {
                if (img2.Key != "Left Glove" && img2.Key != "Right Glove")
                {
                    theDoll.currentDoll.dollClothing[img2.Key].raycastTarget = false;
                }
            }
        }else if (clothesManager.currentCategory == "Shawls" && theDoll.currentDoll.pose == "3Q")
        {
            theDoll.currentDoll.dollClothing["Shawl Front"].raycastTarget = true;

            foreach (KeyValuePair<string, Image> img2 in theDoll.currentDoll.dollClothing)
            {
                if (img2.Key != "Shawl Front")
                {
                    theDoll.currentDoll.dollClothing[img2.Key].raycastTarget = false;
                }
            }
        }else if (clothesManager.currentCategory == "Capes")
        {
            theDoll.currentDoll.dollClothing["Cape Front"].raycastTarget = true;

            foreach (KeyValuePair<string, Image> img2 in theDoll.currentDoll.dollClothing)
            {
                if (img2.Key != "Cape Front")
                {
                    theDoll.currentDoll.dollClothing[img2.Key].raycastTarget = false;
                }
            }
        }
        else
        {
            foreach (KeyValuePair<string, Image> img in theDoll.currentDoll.dollClothing)
            {
                if (img.Key != clothesManager.currentCategory)
                {
                    theDoll.currentDoll.dollClothing[img.Key].raycastTarget = false;
                }
                else
                {
                    theDoll.currentDoll.dollClothing[img.Key].raycastTarget = true;
                }
            }
        }
        hitboxes.HitboxOn();

    }

    //While the item is being dragged
    public void OnDrag(PointerEventData eventData)
    {


        //Item will follow the mouse
        transform.position = new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0.0f);
        //Parent is removed so the object can float around the canvas
        //       transform.parent = canvas.transform;
        transform.SetParent(canvas.transform);

        cGroup.blocksRaycasts = false;

        //If the current target image of the doll doesn't have an existing sprite, the image needs to be turned on.  No color will be added.     
        if (!theCloset.CheckForClothes(theDoll.currentDoll))
        {
            theCloset.ActivateImage();
        }

    }

    //Once the mouse/finger/pointer has released the object
    public void OnEndDrag(PointerEventData eventData)
    {
        hitboxes.HitboxOff();
        //If the pointer is on top of the corresponding current slot, it can be released.
        //Only check otherGloveSlot if the current category is gloves.
        if (clothesManager.currentCategory == "Gloves")
        {
            if (theCloset.currentSlot.mouseOnSlot || theCloset.otherGloveSlot.mouseOnSlot)
            {
                //The doll will change closed based on the index of this drag slot.
                theDoll.ChangeClothes(index);
                //The clothing image will turn on and become fully colored in.
                theCloset.ActivateImage();
                theCloset.ImageColorOn();

                theCloset.currentSlot.hasClothing = true;

                theCloset.otherGloveSlot.hasClothing = true;

            }
            else
            {
                //If the doll didn't dress, turn the image back off.
                //Has Clothing has use if the doll currently has an outfit on, but did not successfully dress with the new clothes.  This way the previous outfit will not be immediately turned off.
                if (!theCloset.currentSlot.hasClothing)
                {
                    theCloset.DeactivateImage();
                }

            }
        }
        else {
            if (theCloset.currentSlot.mouseOnSlot)
            {
                //The doll will change closed based on the index of this drag slot.
                theDoll.ChangeClothes(index);
                //The clothing image will turn on and become fully colored in.
                theCloset.ActivateImage();
                theCloset.ImageColorOn();

                theCloset.currentSlot.hasClothing = true;
            }
            else
            {
                //If the doll didn't dress, turn the image back off.
                //Has Clothing has use if the doll currently has an outfit on, but did not successfully dress with the new clothes.  This way the previous outfit will not be immediately turned off.
                if (!theCloset.currentSlot.hasClothing)
                {
                    theCloset.DeactivateImage();
                }

            }
        }
        //If the pointer is not on top of the corresponding slot, it will reset back to the original position and the doll will not dress.
        
        cGroup.blocksRaycasts = true;
        transform.position = startPosition;

        transform.SetParent(startParent);
 //       transform.parent = startParent;



    }
}
