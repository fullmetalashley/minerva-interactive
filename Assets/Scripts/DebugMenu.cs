﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugMenu : MonoBehaviour
{

    private DebugClock theClock;
    public GameObject debugMenu;

    // Start is called before the first frame update
    void Start()
    {
        theClock = GetComponent<DebugClock>();
    }

    // Update is called once per frame
    void Update()
    {
        //Toggle the clock debug menu.
        
    }

    public void ToggleOnOff()
    {
        debugMenu.SetActive(!debugMenu.activeSelf);
    }
}
