﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class InactiveLetter 
{
    public string sender;
    public DateTime dateActive;
    public string sentDate;
    public string content;
    public bool hasItem;
    public string[] deliveryNames;

    public Sprite[] deliveryIcons;

    public InactiveLetter(DateTime dateActive, string sender, string sentDate, string content)
    {
        this.sender = sender;
        this.dateActive = dateActive;
        this.sentDate = sentDate;
        this.content = content;
        this.hasItem = false;
    }

    public InactiveLetter(DateTime dateActive, string sender, string sentDate, string content, string[] deliveryNames)
    {
        this.sender = sender;
        this.dateActive = dateActive;
        this.sentDate = sentDate;
        this.content = content;
        this.deliveryNames = deliveryNames;
        this.hasItem = true;

        deliveryIcons = new Sprite[deliveryNames.Length];
    }
}
