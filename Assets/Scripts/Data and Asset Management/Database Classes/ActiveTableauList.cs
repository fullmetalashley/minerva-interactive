﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActiveTableauList
{
    public Sprite tableau;

    public ActiveTableauList(Sprite tableau)
    {
        this.tableau = tableau;
    }
}
