﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class InactiveTableauList 
{
    public Sprite tableau;
    public DateTime dateActive;

    public InactiveTableauList(DateTime dateActive, Sprite tableau)
    {
        this.dateActive = dateActive;
        this.tableau = tableau;
    }

    public InactiveTableauList(Sprite tableau)
    {
        this.tableau = tableau;
    }
}
