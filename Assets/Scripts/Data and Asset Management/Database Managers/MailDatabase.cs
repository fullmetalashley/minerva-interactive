﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class MailDatabase : MonoBehaviour
{
    public static MailDatabase control;

    public List<ActiveLetter> unreadMail;
    public List<ActiveLetter> readMail;
    public List<InactiveLetter> inactiveMail;

    private DataManager playerData;

    public int unreadMailLength;
    public int theReadMail;
    public int inactiveMailLength;

    public bool readEstablished;


    void Awake()
    {
        if (control == null)
        {
            DontDestroyOnLoad(gameObject);
            control = this;
        }
        else
        {
            Destroy(gameObject);
        }

    }

    public void InitializeReadMail()
    {
        playerData = FindObjectOfType<DataManager>();
        readMail = new List<ActiveLetter>();
        List<ActiveLetter> tempList = new List<ActiveLetter>();
        if (playerData.unreadLettersProcessed <= unreadMail.Count)
        {
            for (int i = 0; i < playerData.unreadLettersProcessed; i++)
            {
                readMail.Add(unreadMail[i]);
                tempList.Add(unreadMail[i]);
            }
            for (int j = 0; j < tempList.Count; j++)
            {
                unreadMail.Remove(tempList[j]);
            }
        }
    }
    
    //Show each letter in the list, starting at index 0.  Once the button is pressed (i.e. letter is read), mark that letter as read and
    //move it to the read mail list.  Might not even need the bool eventually.
    //NOTE: I do not think this is in use anymore, as of 1/24/2020.
    public void MoveUnreadLetters()
    {
        Debug.Log("Moving mail!");
        for (int i = 0; i < readMail.Count; i++)
        {
            unreadMail.Remove(readMail[i]);
        }
    }

    public void UpdateLists(DateTime currentTime)
    {
        List<InactiveLetter> toBeRemoved = new List<InactiveLetter>();
        foreach (InactiveLetter theLetter in inactiveMail)
        {
            if ((theLetter.dateActive - currentTime).TotalDays < 0 || (theLetter.dateActive - currentTime).TotalDays == 0)
            {

                toBeRemoved.Add(theLetter);
                if (theLetter.hasItem)
                {
                    unreadMail.Add(new ActiveLetter(theLetter.sender, theLetter.sentDate, theLetter.content, true, theLetter.deliveryNames, theLetter.dateActive, theLetter.deliveryIcons));

                }
                else
                {
                    unreadMail.Add(new ActiveLetter(theLetter.sender, theLetter.sentDate, theLetter.content));
                }
            }
        }
        foreach (InactiveLetter inactiveLets in toBeRemoved)
        {
            inactiveMail.Remove(inactiveLets);
        }
    }
}
