﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;

public class AssetLoader : MonoBehaviour
{

    //A string will determine what Act we are in.
    //From there, we will have references built to each folder location.
    //Only populate a folder location in the list if that folder has content.
    //NOTE: empty folders should not be in the resources folder, so any folder that exists should work.
    //AS OF 1/24/2020, act to load will need to change based on player data.  This is not built yet.
    public string actToLoad;

    //References to the clothing locations.
    public string activeClothingLocation;
    public string inactiveClothingLocation;

    //Reference to the saved player tableaus.
    public string savedTableausLocation;

    //References to the tableau location.
    public string activeTableauLocation;
    public string inactiveTableauLocation;

    //References to the jewelry location.
    public string activeJewelryLocation;
    public string inactiveJewelryLocation;

    //References to the letter location.
    public string activeLettersLocation;
    public string inactiveLettersLocation;

    //Stored lists of the specific letter names.  
    public string[] activeLetterNames;
    public string[] inactiveLetterNames;


    //THE FOLLOWING LISTS ARE USED TO ESTABLISH DATABASES.
    //Clothing elements that need to be loaded.
    public List<string> activeClothingElementsToLoad;
    public List<string> inactiveClothingElementsToLoad;

    public List<string> activeJewelryElementsToLoad;
    public List<string> inactiveJewelryElementsToLoad;

    public List<string> act1Dates;
    public List<string> act2Dates;
    public List<string> act3Dates;

    public List<DateTime> act1Times;
    public List<DateTime> act2Times;
    public List<DateTime> act3Times;
        
    public List<string> inactiveDropDates;

    public List<ActiveDatabaseList> activeClothing;
    public List<InactiveDatabaseList> inactiveClothing;

    public List<ActiveTableauList> activeTableaus;
    public List<InactiveTableauList> inactiveTableaus;

    public List<ActiveLetter> activeLetters;
    public List<InactiveLetter> inactiveLetters;

    public List<ActiveDatabaseList> activeJewelry;
    public List<InactiveDatabaseList> inactiveJewelry;


    //These references establish the display icon database.
    public string displayLocation;
    public DisplayItemList displayItems;

    //This is a temporary list used to help establish the display icon list.
    public List<Sprite> tempSprites;


    //References to the text assets for volume 1 of PNP.
    private Dictionary<int, List<List<string>>> pnpText;

    public List<TextAsset> volume1TextAssets;

    //Called by the game initializer.
    //This creates each of the lists and eventually sends them to their respective databases.
    public void InitializeLoad()
    {
        //Lists are all initialized.
        activeClothing = new List<ActiveDatabaseList>();
        inactiveClothing = new List<InactiveDatabaseList>();

        activeTableaus = new List<ActiveTableauList>();
        inactiveTableaus = new List<InactiveTableauList>();

        activeLetters = new List<ActiveLetter>();
        inactiveLetters = new List<InactiveLetter>();

        activeJewelry = new List<ActiveDatabaseList>();
        inactiveJewelry = new List<InactiveDatabaseList>();

        pnpText = new Dictionary<int, List<List<string>>>();

        act1Times = new List<DateTime>();
        act2Times = new List<DateTime>();
        act3Times = new List<DateTime>();

        //Each set of folders in the resource list is parsed and established in their individual lists.
        LoadActiveClothing();
        LoadInactiveClothing();

        LoadActiveJewelry();
        LoadInactiveJewelry();

        LoadActiveTableaus();
        LoadInactiveTableaus();

        LoadDisplayIcons();

        LoadPnPText();

        LoadMessages();

        savedTableausLocation = Application.persistentDataPath;

        tempSprites = new List<Sprite>();
        LoadSavedTableaus();

        EstablishDates(act1Dates, act1Times);
        EstablishDates(act2Dates, act2Times);
        EstablishDates(act3Dates, act3Times);

        EstablishDeliveryIcons();

        //All established lists are sent to their respective database scripts.
        TransferData();
    }

    public void LoadPnPText()
    {
        List<List<string>> volume1Chapters = new List<List<string>>();
        for (int i = 0; i < volume1TextAssets.Count; i++)
        {
            //Each asset is an individual chapter.
            List<string> vol1Chapter = new List<string>();
            string fullChap = volume1TextAssets[i].text;
            string[] splitChap = fullChap.Split("$"[0]); //Split the chapter into an array of strings.

            for (int j = 0; j < splitChap.Length; j++) //For each "page", we add it to the vol1Chapter.
            {
                vol1Chapter.Add(splitChap[j]);
            }
            //Once we're done, we can add this chapter to the overall PnP text.
            volume1Chapters.Add(vol1Chapter);
        }
        pnpText.Add(1, volume1Chapters);
    }

    public void LoadSavedTableaus()
    {
        string[] fileNames = Directory.GetFiles(savedTableausLocation);
        for (int i = 0; i < fileNames.Length; i++)
        {
            if (fileNames[i].Contains("Tableau"))
            {
                //We have found a tableau.
                Texture2D Tex2D; //Turn it into a texture
                byte[] FileData; //We also need to read the data to push it to the texture.

                FileData = File.ReadAllBytes(fileNames[i]);
                Tex2D = new Texture2D(2, 2); //Empty texture
                Tex2D.LoadImage(FileData); //Write the file to the texture

                Sprite NewSprite = Sprite.Create(Tex2D, new Rect(0, 0, Tex2D.width, Tex2D.height), new Vector2(0, 0), 100);
                tempSprites.Add(NewSprite);
            }
        }
    }

    //All display icons are loaded.  This has to happen after clothing / jewelry / tableaus have been established.
    public void LoadDisplayIcons()
    {
        int totalMatches3Q = 0;
        int totalMatchesStraight = 0;
        int mismatches = 0;
        int jewelryMatches3Q = 0;
        int jewelryMatchesStraight = 0;
        int jewelryMismatches = 0;

        Sprite[] tempList = Resources.LoadAll<Sprite>(displayLocation);

        List<string> displayNames = new List<string>();
        for (int i = 0; i < tempList.Length; i++)
        {
            displayNames.Add(tempList[i].name);
        }

        displayItems = new DisplayItemList(displayNames, tempList);

        //DEBUGGING FOR THE REST OF THE METHOD:
        //This is debugging stuff to make sure the matches are all lining up.
        for (int z = 0; z < displayItems.signifier.Count; z++) {
            bool foundMatch = false;
            for (int i = 0; i < activeClothing.Count; i++)
            {
                for (int j = 0; j < activeClothing[i].assetSprites.Count; j++)
                {
                    //We need to break out the signifier from the clothing name.
                    string[] splitString = activeClothing[i].assetSprites[j].name.Split("$"[0]);
                    splitString[splitString.Length - 1] = splitString[splitString.Length - 1].Trim();
                    string currentSig = splitString[splitString.Length - 1];

                    if (currentSig == displayItems.signifier[z])
                    {
                        foundMatch = true;

                        if (activeClothing[i].pose == "3Q"){
                            totalMatches3Q++;
                            
                        }
                        else if (activeClothing[i].pose == "Straight")
                        {
                            totalMatchesStraight++;
                        }                      
                    }
                }
            }
            if (z + 1 == displayItems.signifier.Count)
            {
                //We are about to move to a new signifier.
                if (!foundMatch)
                {
                    mismatches++;
                }
            }
        }

        //This is purely for debugging to make sure the matches are all lining up.
        for (int p = 0; p < displayItems.signifier.Count; p++)
        {
            bool foundMatch = false;
            for (int m = 0; m < activeJewelry.Count; m++)
            {
                for (int g = 0; g < activeJewelry[m].assetSprites.Count; g++)
                {
                    //We need to break out the signifier from the clothing name.
                    string[] splitString = activeJewelry[m].assetSprites[g].name.Split("$"[0]);
                    splitString[splitString.Length - 1] = splitString[splitString.Length - 1].Trim();
                    string currentSig = splitString[splitString.Length - 1];

                    if (currentSig == displayItems.signifier[p])
                    {
                        foundMatch = true;
                        if (activeJewelry[m].pose == "3Q")
                        {
                            jewelryMatches3Q++;

                        }
                        else if (activeJewelry[m].pose == "Straight")
                        {
                            jewelryMatchesStraight++;
                        }
                    }
                }
            }
            if (p + 1 == displayItems.signifier.Count)
            {
                //We are about to move to a new signifier.
                if (!foundMatch)
                {
                    jewelryMismatches++;
                }
            }
        }
    }

    public void LoadActiveClothing()
    {
        for (int i = 0; i < activeClothingElementsToLoad.Count; i++)
        {
            string[] splitString = activeClothingElementsToLoad[i].Split("-"[0]);

            for (int j = 0; j < splitString.Length; j++)
            {
                splitString[j] = splitString[j].Trim();
            }

            activeClothing.Add(new ActiveDatabaseList(splitString[0], splitString[1]));

            Sprite[] tempList = Resources.LoadAll<Sprite>(actToLoad + "/" + activeClothingLocation + "/" + activeClothingElementsToLoad[i]);

            foreach (Sprite spr in tempList)
            {
                activeClothing[i].assetSprites.Add(spr);
            }
        }
    }

    public void LoadInactiveClothing()
    {
        for (int i = 0; i < inactiveClothingElementsToLoad.Count; i++)
        {
            Sprite[] tempList = new Sprite[0];

            string[] splitString = inactiveClothingElementsToLoad[i].Split("-"[0]);

            for (int j = 0; j < splitString.Length; j++)
            {
                splitString[j] = splitString[j].Trim();
            }

            for (int k = 0; k < inactiveDropDates.Count; k++)
            {
                string[] splitDate = inactiveDropDates[k].Split("-"[0]);
                int[] newDate = new int[3];
                for (int m = 0; m < splitDate.Length; m++)
                {
                    newDate[m] = int.Parse(splitDate[m]);
                }

                DateTime newTime = new DateTime(newDate[2], newDate[0], newDate[1]);

                tempList = Resources.LoadAll<Sprite>(actToLoad + "/" + inactiveClothingLocation + "/" + inactiveClothingElementsToLoad[i] + "/" + inactiveDropDates[k]);

                if (tempList.Length != 0)
                {
                    inactiveClothing.Add(new InactiveDatabaseList(newTime, splitString[0], splitString[1]));
                    foreach (Sprite spr in tempList)
                    {
                        inactiveClothing[inactiveClothing.Count - 1].assetSprites.Add(spr);
                    }
                }
            }
        }

    }

    public void LoadActiveJewelry()
    {
        for (int i = 0; i < activeJewelryElementsToLoad.Count; i++)
        {
            string[] splitString = activeJewelryElementsToLoad[i].Split("-"[0]);

            for (int j = 0; j < splitString.Length; j++)
            {
                splitString[j] = splitString[j].Trim();
            }
            activeJewelry.Add(new ActiveDatabaseList(splitString[0], splitString[1]));

            
            Sprite[] tempList = Resources.LoadAll<Sprite>(actToLoad + "/" + activeJewelryLocation + "/" + activeJewelryElementsToLoad[i]);

            foreach (Sprite spr in tempList)
            {
                activeJewelry[i].assetSprites.Add(spr);
            }
        }   
    }

    public void LoadInactiveJewelry()
    {
        for (int i = 0; i < inactiveJewelryElementsToLoad.Count; i++)
        {
            Sprite[] tempList = new Sprite[0];

            string[] splitString = inactiveJewelryElementsToLoad[i].Split("-"[0]);

            for (int j = 0; j < splitString.Length; j++)
            {
                splitString[j] = splitString[j].Trim();
            }

            for (int k = 0; k < inactiveDropDates.Count; k++)
            {
                string[] splitDate = inactiveDropDates[k].Split("-"[0]);
                int[] newDate = new int[3];
                for (int m = 0; m < splitDate.Length; m++)
                {
                    newDate[m] = int.Parse(splitDate[m]);
                }

                DateTime newTime = new DateTime(newDate[2], newDate[0], newDate[1]);

                tempList = Resources.LoadAll<Sprite>(actToLoad + "/" + inactiveJewelryLocation + "/" + inactiveJewelryElementsToLoad[i] + "/" + inactiveDropDates[k]);

                if (tempList.Length != 0)
                {
                    inactiveJewelry.Add(new InactiveDatabaseList(newTime, splitString[0], splitString[1]));
                    foreach (Sprite spr in tempList)
                    {
                        inactiveJewelry[inactiveJewelry.Count - 1].assetSprites.Add(spr);
                    }
                }
            }
        }
    }

    public void EstablishDates(List<string> actToList, List<DateTime> actTimes)
    {
        for (int i = 0; i < actToList.Count; i++)
        {
            String texttt = actToList[i];
            String[] textLines = texttt.Split("-"[0]);

            for (int q = 0; q < textLines.Length; q++)
            {
                textLines[q] = textLines[q].Trim();
            }
            int[] newDate = new int[3];

            for (int p = 0; p < textLines.Length; p++)
            {
                newDate[p] = int.Parse(textLines[p]);
            }

            DateTime newTime = new DateTime(newDate[2], newDate[0], newDate[1]);
            actTimes.Add(newTime);
        }
    }

    public void LoadMessages()
    {
        List<TextAsset> activeLetterList = new List<TextAsset>();
        //Start with the length of the active letters.
        for (int m = 0; m < activeLetterNames.Length; m++)
        {
            TextAsset tempList = (TextAsset)Resources.Load(actToLoad + "/" + activeLettersLocation + "/" + activeLetterNames[m]);
            activeLetterList.Add(tempList);
        }

        for (int i = 0; i < activeLetterList.Count; i++)
        {
            String texttt = activeLetterList[i].text;
            String[] textLines = texttt.Split("$"[0]);

            for (int j = 0; j < textLines.Length; j++)
            {
                textLines[i] = textLines[i].Trim();
            }

            ActiveLetter newLetter = new ActiveLetter(textLines[2], textLines[1], textLines[3]);
            activeLetters.Add(newLetter);
        }

        List<TextAsset> inactiveLetterList = new List<TextAsset>();
        for (int k = 0; k < inactiveLetterNames.Length; k++)
        {
            TextAsset tempList = (TextAsset)Resources.Load(actToLoad + "/" + inactiveLettersLocation + "/" + inactiveLetterNames[k]);
            inactiveLetterList.Add(tempList);
        }

        //Okay, so now we need to go through that list and create the new InactiveLetters.
        for (int m = 0; m < inactiveLetterList.Count; m++)
        {
            String texttt = inactiveLetterList[m].text;
            String[] textLines = texttt.Split("$"[0]);


            for (int j = 0; j < textLines.Length; j++)
            {
                textLines[j] = textLines[j].Trim();
            }


            string[] splitDate = textLines[0].Split("-"[0]);
            for (int q = 0; q < splitDate.Length; q++)
            {
                splitDate[q] = splitDate[q].Trim();
            }
            int[] newDate = new int[3];

            for (int p = 0; p < splitDate.Length; p++)
            {
                newDate[p] = int.Parse(splitDate[p]);
            }

            string[] deliveryStuff = textLines[4].Split("#"[0]);
            for (int z = 0; z < deliveryStuff.Length; z++)
            {
                deliveryStuff[z] = deliveryStuff[z].Trim();
            }

            DateTime tempTime = new DateTime(newDate[2], newDate[0], newDate[1]);
            if (deliveryStuff[0] != "NONE")
            {
                InactiveLetter newLetter = new InactiveLetter(tempTime, textLines[2], textLines[1], textLines[3], deliveryStuff);
                inactiveLetters.Add(newLetter);

            }
            else
            {
                InactiveLetter newLetter = new InactiveLetter(tempTime, textLines[2], textLines[1], textLines[3]);
                inactiveLetters.Add(newLetter);
            }
        } 
    }

    void EstablishDeliveryIcons()
    {
        for (int i = 0; i < inactiveLetters.Count; i++)
        {
            if (inactiveLetters[i].hasItem)
            {
                //This letter has a series of icons.
                //Take the list of icon names, and search each database for it.  
                //Tableau lists first.  We should only ever need to look through inactive lists.
                for (int a = 0; a < inactiveTableaus.Count; a++)
                {
                    for (int b = 0; b < inactiveLetters[i].deliveryNames.Length; b++)
                    {
                        if (inactiveTableaus[a].tableau.name == inactiveLetters[i].deliveryNames[b])
                        {
                            //We have found a sprite match!
                            inactiveLetters[i].deliveryIcons[b] = inactiveTableaus[a].tableau;
                        }
                    }
                }

                //We should do this differently for jewelry and for clothing.
                //Instead, we can cycle through the display list as long as they are parsed first.
                //And if the names is the same as the display list item, set that as the image.
                for (int e = 0; e < inactiveLetters[i].deliveryNames.Length; e++)
                {
                    if (displayItems.signifier.Contains(inactiveLetters[i].deliveryNames[e]))
                    {
                        inactiveLetters[i].deliveryIcons[e] = displayItems.displayIcons[inactiveLetters[i].deliveryNames[e]];
                    }
                }
            }
        }
    }

    public void LoadActiveTableaus()
    {
        //So to load active tableaus, we have the location already.
        Sprite[] tempList = new Sprite[0];
        tempList = Resources.LoadAll<Sprite>(actToLoad + "/" + activeTableauLocation);

        if (tempList.Length != 0)
        {
            for (int i = 0; i < tempList.Length; i++)
            {
                activeTableaus.Add(new ActiveTableauList(tempList[i]));
            }
        }
    }

    public void LoadInactiveTableaus()
    {
        for (int k = 0; k < inactiveDropDates.Count; k++)
        {
            string[] splitDate = inactiveDropDates[k].Split("-"[0]);
            int[] newDate = new int[3];
            for (int m = 0; m < splitDate.Length; m++)
            {
                newDate[m] = int.Parse(splitDate[m]);
            }

            DateTime newTime = new DateTime(newDate[2], newDate[0], newDate[1]);

            Sprite[] tempList = new Sprite[0];
            //For GGC, we are loading from a different folder.
            tempList = Resources.LoadAll<Sprite>(actToLoad + "/" + inactiveTableauLocation + "/" + inactiveDropDates[k]);
            if (tempList.Length != 0)
            {
                for (int i = 0; i < tempList.Length; i++)
                {
                    inactiveTableaus.Add(new InactiveTableauList(newTime, tempList[i]));

                }
            }
        }
    }

    public void TransferData()
    {
        DataManager playerData = FindObjectOfType<DataManager>();
        playerData.savedTableaus = this.tempSprites;

        ClothingDatabase theclothingManager = FindObjectOfType<ClothingDatabase>();
        theclothingManager.activeClothing = this.activeClothing;
        theclothingManager.inactiveClothing = this.inactiveClothing;
        theclothingManager.displayItems = this.displayItems;

        TableauDatabase theTableauManager = FindObjectOfType<TableauDatabase>();
        theTableauManager.activeTableaus = this.activeTableaus;
        theTableauManager.inactiveTableaus = this.inactiveTableaus;

        MailDatabase theMailManager = FindObjectOfType<MailDatabase>();
        theMailManager.unreadMail = this.activeLetters;
        theMailManager.inactiveMail = this.inactiveLetters;

        ClockTracker theClock = FindObjectOfType<ClockTracker>();
        theClock.act1Times = this.act1Times;
        theClock.act2Times = this.act2Times;
        theClock.act3Times = this.act3Times;

        JewelryDatabase theJewelry = FindObjectOfType<JewelryDatabase>();
        theJewelry.activeJewelry = this.activeJewelry;
        theJewelry.inactiveJewelry = this.inactiveJewelry;     

        PnPDatabase pnpNovel = FindObjectOfType<PnPDatabase>();
        pnpNovel.volume1 = this.pnpText;
    }


    //THESE ARE ALL DEBUG METHODS.
    void DebugLetters()
    {
        for (int i = 0; i < inactiveLetters.Count; i++)
        {
            Debug.Log(inactiveLetters[i].sentDate);
            Debug.Log(inactiveLetters[i].content);
            Debug.Log(inactiveLetters[i].dateActive.ToString());
        }
    }

    public void DebugClothingAssets()
    {
        for (int z = 0; z < inactiveClothing.Count; z++)
        {
            Debug.Log("INACTIVE ITEM: " + inactiveClothing[z].itemClass + " Pose: " + inactiveClothing[z].pose + "Drop Date: " + inactiveClothing[z].stringDate);
            foreach (Sprite sprrr in inactiveClothing[z].assetSprites)
            {
                Debug.Log("\nSprite name: " + sprrr.name + " which drops on " + inactiveClothing[z].stringDate);
            }
        }
        for (int z = 0; z < activeClothing.Count; z++)
        {
            Debug.Log("ACTIVE ITEM: " + activeClothing[z].itemClass + " Pose: " + activeClothing[z].pose);
            foreach (Sprite sprrr in activeClothing[z].assetSprites)
            {
                Debug.Log("\nSprite name: " + sprrr.name);
            }
        }
    }

    public void DebugJewelry()
    {
        Debug.Log("Debugging jewelry...");
        for (int z = 0; z < inactiveJewelry.Count; z++)
        {
            Debug.Log("INACTIVE ITEM: " + inactiveJewelry[z].itemClass + " Pose: " + inactiveJewelry[z].pose + "Drop Date: " + inactiveJewelry[z].stringDate);
            foreach (Sprite sprrr in inactiveJewelry[z].assetSprites)
            {
                Debug.Log("\nSprite name: " + sprrr.name + " which drops on " + inactiveJewelry[z].stringDate);
            }
        }
        for (int q = 0; q < activeJewelry.Count; q++)
        {
            Debug.Log("ACTIVE ITEM: " + activeJewelry[q].itemClass + " Pose: " + activeJewelry[q].pose);
            foreach (Sprite sprrr in activeJewelry[q].assetSprites)
            {
                Debug.Log("\nSprite name: " + sprrr.name);
            }
        }
    }

    public void DebugTableaus()
    {
        for (int i = 0; i < activeTableaus.Count; i++)
        {
            Debug.Log("ACTIVE TABLEAU: " + activeTableaus[i].tableau.name);
        }
        for (int m = 0; m < inactiveTableaus.Count; m++)
        {
            Debug.Log("INACTIVE TABLEAU: " + inactiveTableaus[m].tableau.name);
        }
    }
}
