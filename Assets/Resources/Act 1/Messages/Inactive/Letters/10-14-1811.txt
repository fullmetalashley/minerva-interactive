10-14-1811$Longbourn, Hertfordshire.$October 14, 1811.$Beloved friends,

Have you read La Belle Assemblee lately?  There are many wonders in each issue, but I write today because it had lead me towards several items that might be of 
use for you during tomorrow night's ball.  These are fashions pulled directly from the plates, though I highly recommend you peruse the poetry in the most
recent issue.

Warmest regards,
A friend$evedresslavender#eveglovewhitelace#fanlavender