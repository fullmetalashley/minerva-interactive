10-09-1811$Longbourn, Hertfordshire.$October 9, 1811.$Dearest Aunt,
There is much to say about our new neighbors in Netherfield, and depending on who you ask, there is much to say about their status.  Mama is particularly smitten
with the prospect of marrying our sweet Jane to the equally sweet Mr. Bingley; he has quite a fortune, and Jane has the beauty to match.  Perhaps we will know 
more after the upcoming assembly passes and we have a chance to speak with our new friends in person.

Much love,
E. Bennet$goldcomb#dayglovegray